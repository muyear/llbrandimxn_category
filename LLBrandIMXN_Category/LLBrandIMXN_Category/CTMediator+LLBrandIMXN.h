//
//  CTMediator+LLBrandIMXN.h
//  LLBrandIMXN_Category
//
//  Created by muyear on 2018/12/18.
//  Copyright © 2018 muyear. All rights reserved.
//

#import <CTMediator/CTMediator.h>

NS_ASSUME_NONNULL_BEGIN

@interface CTMediator (LLBrandIMXN)
- (void)LLBrandIMXN_startWithSiteid:(NSString *)siteid jPushAppKey:(nullable NSString *)appKey launchOptions:(nullable NSDictionary *)userInfo;
- (void)LLBrandIMXN_registerPushDeviceID:(NSData *)deviceToken;
- (void)LLBrandIMXN_handleRemoteNotification:(NSDictionary *)userInfo;
- (void)LLBrandIMXN_loginInWithUserId:(NSString *)userId userName:(NSString *)userName callback:(void(^)(NSString * loginResult))callback;
- (void)LLBrandIMXN_logout;
- (UIViewController *)LLBrandIMXN_chatViewController:(NSString *)settingId productId:(nullable NSString *)productId;
- (void)LLBrandIMXN_productTrail:(NSString *)productId productName:(NSString *)productName mp:(NSString *)mp sp:(NSString *)sp iu:(NSString *)iu;
- (void)LLBrandIMXN_orderTrail:(NSString *)orderId orderPrice:(NSString *)orderPrice;
- (void)LLBrandIMXN_unReadCount:(void(^)(NSString * unread))callback;
@end

NS_ASSUME_NONNULL_END
