//
//  CTMediator+LLBrandIMXN.m
//  LLBrandIMXN_Category
//
//  Created by muyear on 2018/12/18.
//  Copyright © 2018 muyear. All rights reserved.
//

#import "CTMediator+LLBrandIMXN.h"
NSString * const kCTMediatorTargetLLBrandIMXN = @"LLBrandIMXN";
//初始化
NSString * const kCTMediatorActionStartWithiteid = @"startWithSiteid";

NSString * const kCTMediatorActionRegisterPushDeviceID = @"registerPushDeviceID";
NSString * const kCTMediatorActionHandleRemoteNotification = @"handleRemoteNotification";
NSString * const kCTMediatorActionLoginWithUserId = @"loginWithUserId";
NSString * const kCTMediatorActionLoginOut= @"loginOut";
NSString * const kCTMediatorActionChatViewController= @"chatViewController";
NSString * const kCTMediatorActionProductTrail= @"productTrail";
NSString * const kCTMediatorActionOrderTrail= @"orderTrail";
NSString * const kCTMediatorActionUnReadCount= @"unReadCount";
@implementation CTMediator (LLBrandIMXN)
- (void)LLBrandIMXN_startWithSiteid:(NSString *)siteid jPushAppKey:(nullable NSString *)appKey launchOptions:(nullable NSDictionary *)launchOptions{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"siteid"] = siteid;
    params[@"jPushAppKey"] = appKey;
    params[@"launchOptions"] = launchOptions;
    [self performTarget:kCTMediatorTargetLLBrandIMXN action:kCTMediatorActionStartWithiteid params:params shouldCacheTarget:YES];
}

- (void)LLBrandIMXN_registerPushDeviceID:(NSData *)deviceToken {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"deviceToken"] = deviceToken;
    [self performTarget:kCTMediatorTargetLLBrandIMXN action:kCTMediatorActionRegisterPushDeviceID params:params shouldCacheTarget:NO];
}

- (void)LLBrandIMXN_handleRemoteNotification:(NSDictionary *)userInfo {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"userInfo"] = userInfo;
    [self performTarget:kCTMediatorTargetLLBrandIMXN action:kCTMediatorActionHandleRemoteNotification params:params shouldCacheTarget:NO];
}

- (void)LLBrandIMXN_loginInWithUserId:(NSString *)userId userName:(NSString *)userName callback:(void(^)(NSString * loginResult))callback; {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"userId"] = userId;
    params[@"userName"] = userName;
    params[@"callback"] = callback;
    [self performTarget:kCTMediatorTargetLLBrandIMXN action:kCTMediatorActionLoginWithUserId params:params shouldCacheTarget:NO];
}

- (void)LLBrandIMXN_logout {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [self performTarget:kCTMediatorTargetLLBrandIMXN action:kCTMediatorActionLoginOut params:params shouldCacheTarget:NO];
}
- (UIViewController *)LLBrandIMXN_chatViewController:(NSString *)settingId productId:(nullable NSString *)productId {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"settingId"] = settingId;
    params[@"productId"] = productId;
    return [self performTarget:kCTMediatorTargetLLBrandIMXN action:kCTMediatorActionChatViewController params:params shouldCacheTarget:NO];
}

- (void)LLBrandIMXN_productTrail:(NSString *)productId productName:(NSString *)productName mp:(NSString *)mp sp:(NSString *)sp iu:(NSString *)iu {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"productId"] = productId;
    params[@"productName"] = productName;
    params[@"mp"] = mp;
    params[@"sp"] = sp;
    params[@"iu"] = iu;
    [self performTarget:kCTMediatorTargetLLBrandIMXN action:kCTMediatorActionProductTrail params:params shouldCacheTarget:NO];
}

- (void)LLBrandIMXN_orderTrail:(NSString *)orderId orderPrice:(NSString *)orderPrice {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"orderId"] = orderId;
    params[@"orderPrice"] = orderPrice;
    [self performTarget:kCTMediatorTargetLLBrandIMXN action:kCTMediatorActionOrderTrail params:params shouldCacheTarget:NO];
}

- (void)LLBrandIMXN_unReadCount:(void(^)(NSString * unread))callback {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"callback"] = callback;
    [self performTarget:kCTMediatorTargetLLBrandIMXN action:kCTMediatorActionUnReadCount params:params shouldCacheTarget:NO];
}
@end
